$(document).ready(function() {

	$(".hamburger").click(function() {
		$(".mobile-menu").slideToggle("slow", function() {
			$(".hamburger").hide();
			$(".cross").show();
		});
	});

	$(".cross").click(function() {
		$(".mobile-menu").slideToggle("slow", function() {
			$(".cross").hide();
			$(".hamburger").show();
		});
	});

	var navWatcher = function() {
		if (window.matchMedia('(min-width: 640px)').matches) {
			$(".cross").hide();
			$(".hamburger").hide();
			$(".mobile-menu").hide();
		} else {
			if($(".cross").is(":hidden")) {
				$(".hamburger").show();
			}
		}	
	};

	$(window).resize(navWatcher);

	$(".rslides").responsiveSlides({
		auto: false,
		speed: 1000,
		nav: true,
		prevText: "",
  	nextText: ""
	});

});	